<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Transdent</title>
    <link rel="stylesheet" href="stylesheets/app.css" />
    <script src="bower_components/modernizr/modernizr.js"></script>
  </head>
  <body>
    <div class="row">
      <div class="large-8 columns">
        <h2>Transdent</h2>
      </div>
    </div>
    <div id="hero" class="row">
      <div class="large-12 columns">
        <div class="panel">
          <h2>Call to Action</h2><br><br>
          <p>Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Cras mattis consectetur purus sit amet fermentum. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.</p><br><br>
        </div>
      </div>
    </div>

    <div id="respond" class="row">
      <div class="large-12 columns">
        <form class="panel clearfix" data-abide>
          <div class="small-6 columns name-field">
            <label>Your name <small>required</small>
              <input type="text" required pattern="[a-zA-Z]+">
            </label>
            <small class="error">Name is required and must be a string.</small>
          </div>
          <div class="small-6 columns email-field">
            <label>Email <small>required</small>
              <input type="email" required>
            </label>
            <small class="error">An email address is required.</small>
          </div>
          <button class="clearfix right" type="submit">Submit</button>
        </form>
      </div>
    </div>
    <footer>
      <div id="main-footer">
        <div class="row">
          <div class="large-12 columns">
            <a href="#" class="button regular">Transdent Logo</a>
            <p class="copyright">&copy; 2006&dash;2014 Patterson, Inc. All rights reserved. <a href="#">Privacy Policy</a> | <a href="#">Terms &amp; Conditions</a></p>
          </div>
        </div>
      </div>
    </footer>

    <script src="bower_components/jquery/dist/jquery.min.js"></script>
    <script src="bower_components/foundation/js/foundation.min.js"></script>
    <script src="js/app.js"></script>
  </body>
</html>
