<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Transdent</title>
    <link rel="stylesheet" href="stylesheets/app.css" />
    <script src="bower_components/modernizr/modernizr.js"></script>
  </head>
  <body>
    <?php include 'header.php'; ?>
    <div id="filters" class="row">
      <h1 class="large-12 columns">Search the Transdent Network</h1>
      <div class="large-12 columns">
        <div class="panel">
          <form>
            <div class="row">
              <label class="small-4 columns">I want to find 
                <select class="small-8">
                  <option value="associate">an associate who</option>
                  <option value="practice">a practice that</option>
                </select>
              </label>
              <label class="small-4 columns left">specializes in  
                <select class="small-7">
                  <option value="all" >any specialty</option>
                  <option value="endodontistry" >endodontistry</option>
                  <option value="general dentistry" >general dentistry</option>
                  <option value="oral surgery" >oral surgery</option>
                  <option value="orthodontistry" >orthodontistry</option>
                  <option value="pediatric dentistry" >pediatric dentistry</option>
                  <option value="periodontistry" >periodontistry</option>
                  <option value="prosthodontistry" >prosthodontistry</option>
                </select>
              </label>
            </div>
            <div class="row">
              <hr>
              <div class="small-12 columns text-center">
                <h5> - in or near - </h5>
              </div>
              <hr>
              <div class="small-6 columns">
                <input type="text" id="city" placeholder="City - Start Typing">
              </div>
              <div class="small-6 columns">
                <input type="text" id="zipcode" placeholder="Zip Code">
              </div>
            </div>
            <input class="button small expand" href="#" type="submit" value="Search">
          </form>
        </div>
      </div>
    </div>

    <div class="row clearfix">
      <div class="large-12 columns">
        <style type="text/css">
          #map {
            width: 100%;
            height: 400px;
          }
        </style>
        <iframe id="map" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d90325.44295107712!2d-93.26185345!3d44.97069699999991!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x52b333909377bbbd%3A0x939fc9842f7aee07!2sMinneapolis%2C+MN!5e0!3m2!1sen!2sus!4v1399450163247" width="960" height="500" frameborder="0" style="border:0"></iframe>
        </div>
      </div>
    </div>
    <section id="results" class="row">
      <div class="large-12 columns">
        <h4>Search Results</h4>
        <h5>3 Results for Associates specializing in general dentistry in Minneapolis</h5>
        <button class="tiny">Save Search</button> By saving the search, you will be notified when new listings are added.
        <table>
          <thead>
            <tr>
              <th>Picture</th>
              <th width="275">Practice ID</th>
              <th width="275">Posted/Last Updated</th>
              <th width="275">Send Message</th>
              <th width="275">Favorite</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td><div class="button">Image</div></td>
              <td><a href="#">MN55930G</a></td>
              <td><a href="#">2-18-2014</a></td>
              <td><a href="#">Message</a></td>
              <td><a href="#">Favorite</a></td>
            </tr>
            <tr>
              <td><div class="button">Image</div></td>
              <td><a href="#">MN55930h</a></td>
              <td><a href="#">2-18-2014</a></td>
              <td><a href="#">Message</a></td>
              <td><a href="#">Favorite</a></td>
            </tr>
            <tr>
              <td><div class="button">Image</div></td>
              <td><a href="#">MN55930i</a></td>
              <td><a href="#">2-18-2014</a></td>
              <td><a href="#">Message</a></td>
              <td><a href="#">Favorite</a></td>
            </tr>
          </tbody>
        </table>
      </div>
    </section>
    <?php include 'footer.php'; ?>

    <script src="bower_components/jquery/dist/jquery.min.js"></script>
    <script src="bower_components/foundation/js/foundation.min.js"></script>
    <script src="js/app.js"></script>
  </body>
</html>
