    <div class="row">
      <div class="large-8 columns">
        <h2>Transdent Logo</h2>
      </div>
      <div class="large-4 columns">
        <a class="right button small radius" href="#">Member Login</a>
        <div class="row collapse">
          <div class="small-10 columns">
            <input type="text" placeholder="Search Topic">
          </div>
          <div class="small-2 columns">
            <a href="#" class="button postfix">Search</a>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="large-12 columns">
        <div class="contain-to-grid sticky">
          <nav class="top-bar" data-topbar data-options="sticky_on: large">
            <ul class="title-area">
              <li class="name">
                <h1><a href="#">Home</a></h1>
              </li>
               <!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
              <li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
            </ul>

            <section class="top-bar-section">
              <ul class="right">
                <li class="active has-dropdown">
                  <a href="/aboutus.php">About Us</a>
                  <ul class="dropdown">
                    <li><a href="#">Our Approach</a></li>
                    <li><a href="page-team.php">Our Team &amp; Experience</a></li>
                    <li><a href="page-testimonials.php">Client Testimonials</a></li>
                  </ul>
                </li>
                <li class="has-dropdown">
                  <a href="#">Services</a>
                  <ul class="dropdown">
                    <li><a href="#">Practice Transitions</a></li>
                    <li><a href="#">Practice Sales</a></li>
                    <li><a href="#">Associate Placement</a></li>
                    <li><a href="#">Buyer Representation</a></li>
                    <li><a href="#">Partnership Coaching</a></li>
                    <li><a href="#">Other</a></li> 
                  </ul>
                </li>
                <li class="has-dropdown">
                  <a href="#">For Practice Owners</a>
                  <ul class="dropdown">
                    <li><a href="#">Find an Associate</a></li>
                    <li class="has-dropdown">
                      <a href="#">Sell your Practice</a>
                      <ul class="dropdown">
                        <li><a href="#">Is a transition right for you?</a></li>
                        <li><a href="#">Get Started</a></li>
                      </ul>
                    </li>
                    <li><a href="#">Brokerage Info</a></li>
                  </ul>
                </li>
                <li class="has-dropdown">
                  <a href="#">For Associates</a>
                  <ul class="dropdown">
                    <li><a href="#">Find a Job</a></li>
                    <li class="has-dropdown">
                      <a href="#">Buy a Practice</a>
                      <ul class="dropdown">
                        <li><a href="#">Immediate Purchase</a></li>
                        <li><a href="#">Transition Purchase</a></li>
                      </ul>
                    </li>
                    <li><a href="#">New Doctors Support</a></li>
                    <li><a href="#">Knowledge Center</a></li>
                    <li><a href="#">onTrack</a></li>
                  </ul>
                </li>
                <li><a href="#">Student Services</a></li>
                <li><a href="#">Contact Us</a></li>
              </ul>

            </section>
          </nav>
        </div>
      </div>
    </div>
