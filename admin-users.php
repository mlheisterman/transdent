<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Transdent</title>
    <link rel="stylesheet" href="stylesheets/app.css" />
    <script src="bower_components/modernizr/modernizr.js"></script>
  </head>
  <body>
    <?php include 'header.php'; ?>
    <div id="filters" class="row" data-equalizer>
      <div class="row">
        <h1 class="large-2 columns">Users</h1>
        <a href="admin-dashboard.php" class="left back-to">&laquo; Back to Dashboard</a>
      </div>
      <form class="callout panel radius clearfix">
        <p>User Filter</p>
        <label class="small-4 columns left">User Type  
          <select class="small-7">
            <option value="all" >any type</option>
            <option value="endodontistry" >Practice Owner</option>
            <option value="general dentistry" >Associate</option>
          </select>
        </label>
        <label class="small-4 columns left">Status  
          <select class="small-7">
            <option value="all" >any status</option>
            <option value="endodontistry" >Active</option>
            <option value="general dentistry" >Inactive</option>
          </select>
        </label>
        <label class="small-4 columns left">Specialty  
          <select class="small-7">
            <option value="all" >any specialty</option>
            <option value="endodontistry" >endodontistry</option>
            <option value="general dentistry" >general dentistry</option>
            <option value="oral surgery" >oral surgery</option>
            <option value="orthodontistry" >orthodontistry</option>
            <option value="pediatric dentistry" >pediatric dentistry</option>
            <option value="periodontistry" >periodontistry</option>
            <option value="prosthodontistry" >prosthodontistry</option>
          </select>
        </label>
        <div class="row">
          <div class="small-3 left columns">
            <input type="text" id="city" placeholder="Current City, State, Zip, Etc.">
          </div>
          <div class="small-3 left columns">
            <input type="text" id="city" placeholder="Future City, State, Zip, Etc.">
          </div>
          <div class="small-3 left columns">
            <input type="text" id="city" placeholder="Dental School">
          </div>
          <div class="small-3 left columns">
            <input type="text" id="city" placeholder="Patterson Rep">
          </div>
          <div class="small-3 left columns">
            <label>Last Login</label><input type="date" id="city" placeholder="Last Login">
          </div>
          <div class="small-6 columns">
            <div class="row collapse">
              <div class="small-10 columns">
                <input type="text" placeholder="User Name or ID">
              </div>
              <div class="small-2 columns">
                <a href="#" class="button postfix">Search</a>
              </div>
            </div>
          </div>
        </div>
      </form>
    </div>
    <section id="users" class="row">
      <div class="large-12 columns">
        <h4>User List</h4>
        <form>
          <select class="large-4 columns">
            <option value="activate">Activate User</option>
            <option value="deactivate">Dectivate User</option>
            <option value="export">Export to CSV</option>
          </select>
          <button class="large-1 columns left tiny" type="submit">Submit</button>
        </form>
        <table class="clearfix">
          <thead>
            <tr>
              <th>Action</th>
              <th>Type</th>
              <th width="275">Transdent ID</th>
              <th width="275">Name</th>
              <th width="275">Location</th>
              <th width="275">Specialty</th>
              <th width="275">Created</th>
              <th width="275">Updated</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td><input id="action1" type="checkbox"></td>
              <td>Associate</td>
              <td><a href="#">MN55930G</a></td>
              <td><a href="#">Shayne</a></td>
              <td><a href="#">Scotsdale (23101)</a></td>
              <td><a href="#">General</a></td>
              <td><a href="#">2-18-2014</a></td>
              <td><a href="#">2-18-2014</a></td>
            </tr>
            <tr>
              <td><input id="action1" type="checkbox"></td>
              <td>Practice</td>
              <td><a href="#">MN55930h</a></td>
              <td><a href="#">Dr. Euert</a></td>
              <td><a href="#">Austin (78739)</a></td>
              <td><a href="#">General</a></td>
              <td><a href="#">2-18-2014</a></td>
              <td><a href="#">2-18-2014</a></td>
            </tr>
            <tr>
              <td><input id="action1" type="checkbox"></td>
              <td>Associate</td>
              <td><a href="#">MN55930i</a></td>
              <td><a href="#">Dr. Knight</a></td>
              <td><a href="#">Austin (78739)</a></td>
              <td><a href="#">General</a></td>
              <td><a href="#">2-18-2014</a></td>
              <td><a href="#">2-18-2014</a></td>
            </tr>
          </tbody>
        </table>
        <ul class="pagination">
          <li class="arrow unavailable"><a href="">&laquo;</a></li>
          <li class="current"><a href="">1</a></li>
          <li><a href="">2</a></li>
          <li><a href="">3</a></li>
          <li><a href="">4</a></li>
          <li class="unavailable"><a href="">&hellip;</a></li>
          <li><a href="">12</a></li>
          <li><a href="">13</a></li>
          <li class="arrow"><a href="">&raquo;</a></li>
        </ul>
      </div>
    </section>
    <?php include 'footer.php'; ?>

    <script src="bower_components/jquery/dist/jquery.min.js"></script>
    <script src="bower_components/foundation/js/foundation.min.js"></script>
    <script src="js/app.js"></script>
  </body>
</html>
