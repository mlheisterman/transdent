<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Transdent</title>
    <link rel="stylesheet" href="stylesheets/app.css" />
    <script src="bower_components/modernizr/modernizr.js"></script>
  </head>
  <body>
    <?php include 'header.php'; ?>
        <div id="hero" class="row" data-equalizer>
      <div class="large-3 columns">
        <div class="photograph saustindent" data-equalizer-watch>
        </div>
      </div>
      <div class="large-6 columns">
        <div class="panel" data-equalizer-watch>
          <h4>Dental Practice AUS10564PR</h4>
          <ul>
            <li>Location: Austin, TX</li>
            <li>Specialty: General Densitry</li>
            <li>Owner Graduation: 5-25-1976 - University of Pennsylvania</li>
            <li>Years in Dentistry: 38</li>
          </ul>
        </div>
      </div>
      <div class="large-3 columns">
        <style type="text/css">
          #map {
            width: 100%;
            height: 200px;
          }
        </style>
        <iframe id="map" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d90325.44295107712!2d-93.26185345!3d44.97069699999991!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x52b333909377bbbd%3A0x939fc9842f7aee07!2sMinneapolis%2C+MN!5e0!3m2!1sen!2sus!4v1399450163247" width="300" height="200" frameborder="0" style="border:0"></iframe>
        </div>
      </div>
    </div>

    <div class="row clearfix" data-equalizer>
      <div class="large-4 medium-4 columns" data-equalizer-watch>
        <Button class="tiny expand">Message this Practice</Button>
      </div>
      <div class="large-4 medium-4 columns" data-equalizer-watch>
        <Button class="tiny expand">Bookmark this Practice</Button>
      </div>
      <div class="large-4 medium-4 columns" data-equalizer-watch>
        <Button href="search.php" class="tiny expand">See Similar Practices</Button>
      </div>
    </div>
    <div class="row clearfix">
      <div class="large-12 columns">
        <h4>Practice is Looking For</h4>
          <ul>
            <li>Type of Position: Associate leading to Ownership</li>
            <li>Specialty: General Densitry</li>
            <li>Austin, TX Area</li>
          </ul>
      </div>
    </div>
    <div class="row clearfix">
      <div class="large-12 columns">
        <h4>About Practice</h4>
        <p>Vestibulum id ligula porta felis euismod semper. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Donec sed odio dui. Curabitur blandit tempus porttitor. Praesent commodo cursus magna, vel scelerisque nisl consectetur et.</p>
        <p>Sed posuere consectetur est at lobortis. Maecenas faucibus mollis interdum. Sed posuere consectetur est at lobortis. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
        <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam porta sem malesuada magna mollis euismod. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
      </div>
    </div>
    <?php include 'footer.php'; ?>

    <script src="bower_components/jquery/dist/jquery.min.js"></script>
    <script src="bower_components/foundation/js/foundation.min.js"></script>
    <script src="js/app.js"></script>
  </body>
</html>
