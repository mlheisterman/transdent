<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Transdent</title>
    <link rel="stylesheet" href="stylesheets/app.css" />
    <script src="bower_components/modernizr/modernizr.js"></script>
  </head>
  <body>
    <?php include 'header.php'; ?>
    <div id="filters" class="row" data-equalizer>
      <div class="row">
        <h1 class="large-3 columns">Content</h1>
        <a href="admin-dashboard.php" class="left back-to">&laquo; Back to Dashboard</a>
      </div>
      <form class="callout panel radius clearfix">
        <p>Content Filter</p>
        <label class="small-4 columns left">Content Type  
          <select class="small-7">
            <option value="all" >any type</option>
            <option value="page" >Page</option>
            <option value="webform" >Webform</option>
          </select>
        </label>
        <label class="small-4 columns left">Status  
          <select class="small-7">
            <option value="all" >any status</option>
            <option value="endodontistry" >Published</option>
            <option value="general dentistry" >Unpublished</option>
          </select>
        </label>
        <div class="row">
          <div class="small-3 left columns">
            <input type="text" id="taxonomy" placeholder="Taxonomy Term">
          </div>
          <div class="small-6 columns">
            <div class="row collapse">
              <div class="small-10 columns">
                <input type="text" placeholder="User Name or ID">
              </div>
              <div class="small-2 columns">
                <a href="#" class="button postfix">Search</a>
              </div>
            </div>
          </div>
        </div>
      </form>
    </div>
    <section id="users" class="row">
      <div class="large-12 columns">
        <h4>Content List</h4>
        <table class="clearfix">
          <thead>
            <tr>
              <th>Type</th>
              <th width="275">Title</th>
              <th width="275">Created</th>
              <th width="275">Updated</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Page</td>
              <td><a href="#">About Us</a></td>
              <td><a href="#">2-18-2014</a></td>
              <td><a href="#">2-18-2014</a></td>
            </tr>
            <tr>
              <td>FAQ</td>
              <td><a href="#">Why Transdent</a></td>
              <td><a href="#">2-18-2014</a></td>
              <td><a href="#">2-18-2014</a></td>
            </tr>
            <tr>
              <td>Webform</td>
              <td><a href="#">The big how-to</a></td>
              <td><a href="#">2-18-2014</a></td>
              <td><a href="#">2-18-2014</a></td>
            </tr>
          </tbody>
        </table>
        <ul class="pagination">
          <li class="arrow unavailable"><a href="">&laquo;</a></li>
          <li class="current"><a href="">1</a></li>
          <li><a href="">2</a></li>
          <li><a href="">3</a></li>
          <li><a href="">4</a></li>
          <li class="unavailable"><a href="">&hellip;</a></li>
          <li><a href="">12</a></li>
          <li><a href="">13</a></li>
          <li class="arrow"><a href="">&raquo;</a></li>
        </ul>
      </div>
    </section>
    <?php include 'footer.php'; ?>

    <script src="bower_components/jquery/dist/jquery.min.js"></script>
    <script src="bower_components/foundation/js/foundation.min.js"></script>
    <script src="js/app.js"></script>
  </body>
</html>
