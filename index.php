<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Transdent</title>
    <link rel="stylesheet" href="stylesheets/app.css" />
    <script src="bower_components/modernizr/modernizr.js"></script>
  </head>
  <body>
    <?php include 'header.php'; ?>
    <div id="hero" class="row" data-equalizer>
      <div class="large-8 columns">
        <div class="panel" data-equalizer-watch>
          <h2>Call to Action</h2><br><br>
          <p>Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Cras mattis consectetur purus sit amet fermentum. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.</p><br><br>
        </div>
      </div>
      <div class="large-4 columns">
        <div class="panel" data-equalizer-watch>
          <h4>Search for Practices or Associates</h4>
          <form>
            <div class="row">
              <div class="small-12 columns">
                <label>I want to find 
                  <select class="small-7">
                    <option value="associate">an associate who</option>
                    <option value="practice">a practice that</option>
                  </select>
                </label>
              </div>
            </div>
            <div class="row">
              <div class="small-12 columns">
                <label>specializes in  
                  <select class="small-7">
                    <option value="endodontistry" >endodontistry</option>
                    <option value="general dentistry" >general dentistry</option>
                    <option value="oral surgery" >oral surgery</option>
                    <option value="orthodontistry" >orthodontistry</option>
                    <option value="pediatric dentistry" >pediatric dentistry</option>
                    <option value="periodontistry" >periodontistry</option>
                    <option value="prosthodontistry" >prosthodontistry</option>
                  </select>
                </label>
              </div>
            </div>
            <div class="row">
              <div class="small-4 columns">
                <label for="city" class="inline">in or near </label>
              </div>
              <div class="small-4 columns">
                <input type="text" id="city" placeholder="City">
              </div>
              <div class="small-4 columns">
                <label>,   
                  <select class="small-10">
                    <option value="none" >State</option>
                    <option value="AL">Alabama</option>
                    <option value="AK">Alaska</option>
                    <option value="AZ">Arizona</option>
                    <option value="AR">Arkansas</option>
                    <option value="CA">California</option>
                    <option value="CO">Colorado</option>
                    <option value="CT">Connecticut</option>
                    <option value="DE">Delaware</option>
                    <option value="DC">District Of Columbia</option>
                    <option value="FL">Florida</option>
                    <option value="GA">Georgia</option>
                    <option value="HI">Hawaii</option>
                    <option value="ID">Idaho</option>
                    <option value="IL">Illinois</option>
                    <option value="IN">Indiana</option>
                    <option value="IA">Iowa</option>
                    <option value="KS">Kansas</option>
                    <option value="KY">Kentucky</option>
                    <option value="LA">Louisiana</option>
                    <option value="ME">Maine</option>
                    <option value="MD">Maryland</option>
                    <option value="MA">Massachusetts</option>
                    <option value="MI">Michigan</option>
                    <option value="MN">Minnesota</option>
                    <option value="MS">Mississippi</option>
                    <option value="MO">Missouri</option>
                    <option value="MT">Montana</option>
                    <option value="NE">Nebraska</option>
                    <option value="NV">Nevada</option>
                    <option value="NH">New Hampshire</option>
                    <option value="NJ">New Jersey</option>
                    <option value="NM">New Mexico</option>
                    <option value="NY">New York</option>
                    <option value="NC">North Carolina</option>
                    <option value="ND">North Dakota</option>
                    <option value="OH">Ohio</option>
                    <option value="OK">Oklahoma</option>
                    <option value="OR">Oregon</option>
                    <option value="PA">Pennsylvania</option>
                    <option value="RI">Rhode Island</option>
                    <option value="SC">South Carolina</option>
                    <option value="SD">South Dakota</option>
                    <option value="TN">Tennessee</option>
                    <option value="TX">Texas</option>
                    <option value="UT">Utah</option>
                    <option value="VT">Vermont</option>
                    <option value="VA">Virginia</option>
                    <option value="WA">Washington</option>
                    <option value="WV">West Virginia</option>
                    <option value="WI">Wisconsin</option>
                    <option value="WY">Wyoming</option>
                  </select>
                </label>
              </div>
            </div>
            <input class="button small expand" href="#" type="submit" value="Search">
          </form>
        </div>
      </div>
    </div>

    <div class="row clearfix" data-equalizer>
      <div class="large-4 medium-4 columns" data-equalizer-watch>
        <h4>Our Mission</h4>
        <p>Successfully matching Dental Practices and Associates Online since 2004. Looking for a practice? Create a free Associate Profile and start searching in just minutes. Need an associate? Create a profile for your practice and bring the associates to you. Begin your search for a prosperous new partnership today!</p>
      </div>
      <div class="large-4 medium-4 columns" data-equalizer-watch>
        <h4>Testimonials</h4>
        <blockquote>Those people who think they know everything are a great annoyance to those of us who do.<cite>Isaac Asimov</cite></blockquote>
      </div>
      <div class="large-4 medium-4 columns" data-equalizer-watch>
        <h4>Featured Practice</h4>
        <p>Successfully matching Dental Practices and Associates Online since 2004. Looking for a practice? Create a free Associate Profile and start searching in just minutes. Need an associate? Create a profile for your practice and bring the associates to you. Begin your search for a prosperous new partnership today!</p>
      </div>
    </div>

    <div class="row clearfix">
      <div class="large-12 columns">
        <h4>Nearby Associates &amp; Practices</h4>
        <style type="text/css">
          #map {
            width: 100%;
            height: 400px;
          }
        </style>
        <iframe id="map" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d90325.44295107712!2d-93.26185345!3d44.97069699999991!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x52b333909377bbbd%3A0x939fc9842f7aee07!2sMinneapolis%2C+MN!5e0!3m2!1sen!2sus!4v1399450163247" width="960" height="500" frameborder="0" style="border:0"></iframe>
        </div>
      </div>
    </div>
    <?php include 'footer.php'; ?>

    <script src="bower_components/jquery/dist/jquery.min.js"></script>
    <script src="bower_components/foundation/js/foundation.min.js"></script>
    <script src="js/app.js"></script>
  </body>
</html>
