<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Transdent</title>
    <link rel="stylesheet" href="stylesheets/app.css" />
    <script src="bower_components/modernizr/modernizr.js"></script>
  </head>
  <body>
    <?php include 'header.php'; ?>
    <div id="filters" class="row" data-equalizer>
      <h1 class="large-12 columns">Dr. Euer's Dashboard</h1>
      <div class="large-9 columns" data-equalizer-watch>
        <div data-alert class="alert-box info radius">You have 6 new messages
          <a href="#" class="close">&times;</a>
        </div>
        <div data-alert class="alert-box info radius">3 new listings in your area
          <a href="#" class="close">&times;</a>
        </div>
        <div data-alert class="alert-box warning radius">Your profile will go dormant in 3 days
          <a href="#" class="close">&times;</a>
        </div>
      </div>
      <div class="large-3 columns" data-equalizer-watch>
        <h4>Quick Links</h4>
        <button class="button tiny expand">Edit My Profile</button>
        <button href="search.php" class="button tiny expand">Search Practices &amp; Associates</button>
        <button class="button tiny expand">Send Message</button>
      </div>
    </div>
    <div class="row clearfix" data-equalizer>
      <div class="large-6 medium-6 columns" data-equalizer-watch>
        <h4>Bookmarked Profiles</h4>
        <table>
          <thead>
            <tr>
              <th>ID</th>
              <th>Location</th>
              <th>Description</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td><a href="#">MN55930G</a></td>
              <td><a href="#">Austin</a></td>
              <td><a href="#">I liked this guy.  Lots of experience.  A real mentor possibility.</a></td>
            </tr>
            <tr>
              <td><a href="#">MN55930h</a></td>
              <td><a href="#">Minneapolis</a></td>
              <td><a href="#">This practice is a great candidate to take over.</a></td>
            </tr>
            <tr>
              <td><a href="#">MN55930i</a></td>
              <td><a href="#">Chicago</a></td>
              <td><a href="#">Bleh!</a></td>
            </tr>
          </tbody>
        </table>
      </div>
      <div class="large-6 medium-6 columns" data-equalizer-watch>
        <h4>Newest Messages</h4>
        <table>
          <thead>
            <tr>
              <th>ID</th>
              <th>Date Sent</th>
              <th>Subject</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td><a href="#">MN55930G</a></td>
              <td>2-18-2014</td>
              <td>Please contact me.  I would love to talk about a transition.</td>
            </tr>
            <tr>
              <td><a href="#">MN55930h</a></td>
              <td>2-18-2014</td>
              <td>RE: How many employees do you have?</td>
            </tr>
            <tr>
              <td><a href="#">MN55930i</a></td>
              <td>2-18-2014</td>
              <td>How did you like the University of Pennsylvania?</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
    <section id="saved-searches" class="row">
      <div class="large-12 columns">
        <h4>Saved Searches</h4>
        <p>If anything new is added in these areas, I will get a notification.
        <table>
          <thead>
            <tr>
              <th>Location</th>
              <th width="275">Specialty</th>
              <th width="275">Type</th>
              <th width="275">Action</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Austin</td>
              <td><a href="#">General Dentistry</a></td>
              <td><a href="#">Practice</a></td>
              <td><a href="#">Delete</a></td>
            </tr>
            <tr>
              <td>San Antonio</td>
              <td><a href="#">General Dentistry</a></td>
              <td><a href="#">Practice</a></td>
              <td><a href="#">Delete</a></td>
            </tr>
            <tr>
              <td>Texas</td>
              <td><a href="#">General Dentistry</a></td>
              <td><a href="#">Practice</a></td>
              <td><a href="#">Delete</a></td>
            </tr>
          </tbody>
        </table>
      </div>
    </section>
    
    <?php include 'footer.php'; ?>

    <script src="bower_components/jquery/dist/jquery.min.js"></script>
    <script src="bower_components/foundation/js/foundation.min.js"></script>
    <script src="js/app.js"></script>
  </body>
</html>
