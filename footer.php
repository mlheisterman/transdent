    <footer>
      <div id="newsletter">
        <div class="row">
          <div class="medium-8 columns">
            <h5>Stay on top of what&rsquo;s happening in <a href="#">dentistry</a>.</h5>
            <p>Sign up to receive monthly practice transitions highlights. <a href="#">Read Last Month's Edition &raquo;</a></p>
          </div>
          <div class="medium-4 columns">
            <form>
              <div class="row collapse margintop-20px">
                <div class="small-8 medium-8 columns">
                  <input type="text" name="email" placeholder="signup@example.com">
                </div>
                <div class="small-4 medium-4 columns">
                  <input type="submit" href="#" class="postfix small button expand" value="Sign Up">
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      <div id="main-footer">
        <div class="row">
          <div class="large-12 columns">
            <div class="row">
              <div class="medium-4 medium-4 push-8 columns">
                <ul class="inline-list right">
                    <li><a href="#" class="button tiny twitter">Twitter</a></li>
                    <li><a href="#" class="button tiny facebook">Facebook</a></li>
                    <li><a href="#" class="button tiny mail">Email</a></li>
                  </ul>
               </div>
               <div class="medium-8 medium-8 pull-4 columns">
                  <a href="#" class="button regular">Transdent Logo</a>
                  <ul id="footer-nav" class="inline-list">
                    <li><a href="/aboutus.php">About Us</a></li>
                    <li><a href="#">Services</a></li>
                    <li><a href="#">For Practice Owners</a></li>
                    <li><a href="#">For Associates</a></li>
                    <li><a href="#">Student Services</a></li>
                    <li><a href="#">Contact Us</a></li>
                 </ul>
                 <p class="copyright">&copy; 2006&dash;2014 Patterson, Inc. All rights reserved. <a href="#">Privacy Policy</a> | <a href="#">Terms &amp; Conditions</a></p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </footer>