<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Transdent</title>
    <link rel="stylesheet" href="stylesheets/app.css" />
    <script src="bower_components/modernizr/modernizr.js"></script>
  </head>
  <body>
    <?php include 'header.php'; ?>
    <div id="filters" class="row" data-equalizer>
      <h1 class="large-12 columns">Shayne's Dashboard</h1>
      <div class="large-9 columns" data-equalizer-watch>
        <div data-alert class="alert-box info radius">You have 6 new users this week
          <a href="#" class="close">&times;</a>
        </div>
        <div data-alert class="alert-box info radius">You have 5 new messages this week
          <a href="#" class="close">&times;</a>
        </div>
        <div data-alert class="alert-box info radius">7 profiles have gone inactive this week
          <a href="#" class="close">&times;</a>
        </div>
      </div>
      <div class="large-3 columns" data-equalizer-watch>
        <button class="button small expand">Create Page</button>
        <button class="button small expand">Create Webform/Landing Page</button>
        <button class="button small expand">Create FAQ</button>
        <button class="button small expand">Create Message</button>
      </div>
    </div>
    <div class="row clearfix" data-equalizer>
      <div class="large-4 medium-4 columns" data-equalizer-watch>
        <h4>Help Requests</h4>
        <table>
          <thead>
            <tr>
              <th>ID</th>
              <th>Subject</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td><a href="#">MN55930G</a></td>
              <td><a href="#">I love Transdent</a></td>
              <td><a href="#">View</a></td>
            </tr>
            <tr>
              <td><a href="#">MN55930h</a></td>
              <td><a href="#">I love Transdent</a></td>
              <td><a href="#">View</a></td>
            </tr>
            <tr>
              <td><a href="#">MN55930i</a></td>
              <td><a href="#">Broken Link</a></td>
              <td><a href="#">View</a></td>
            </tr>
          </tbody>
        </table>
      </div>
      <div class="large-4 medium-4 columns" data-equalizer-watch>
        <h4>Recent Transactions</h4>
        <a href="#">View All</a>
        <table>
          <thead>
            <tr>
              <th>ID</th>
              <th>Amount ($)</th>
              <th>Date</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td><a href="#">MN55930G</a></td>
              <td>$200.00</td>
              <td>2-18-2014</td>
            </tr>
            <tr>
              <td><a href="#">MN55930h</a></td>
              <td>$200.00</td>
              <td>2-18-2014</td>
            </tr>
            <tr>
              <td><a href="#">MN55930i</a></td>
              <td>$200.00</td>
              <td>2-18-2014</td>
            </tr>
          </tbody>
        </table>
      </div>
      <div class="large-4 medium-4 columns" data-equalizer-watch>
        <h4>Newest Users</h4>
        <table>
          <thead>
            <tr>
              <th>ID</th>
              <th>Date Joined</th>
              <th>Last Login</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td><a href="#">MN55930G</a></td>
              <td>2-18-2014</td>
              <td>2-18-2014</td>
            </tr>
            <tr>
              <td><a href="#">MN55930h</a></td>
              <td>2-18-2014</td>
              <td>2-18-2014</td>
            </tr>
            <tr>
              <td><a href="#">MN55930i</a></td>
              <td>2-18-2014</td>
              <td>2-18-2014</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
    <section id="content-list" class="row">
      <div class="large-12 columns">
        <h4>Recent Content List</h4>
        <table>
          <thead>
            <tr>
              <th>Type</th>
              <th width="275">Title</th>
              <th width="275">Created</th>
              <th width="275">Updated</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Page</td>
              <td><a href="#">About Us</a></td>
              <td><a href="#">2-18-2014</a></td>
              <td><a href="#">2-18-2014</a></td>
            </tr>
            <tr>
              <td>FAQ</td>
              <td><a href="#">MN55930h</a></td>
              <td><a href="#">2-18-2014</a></td>
              <td><a href="#">2-18-2014</a></td>
            </tr>
            <tr>
              <td>Webform</td>
              <td><a href="#">MN55930i</a></td>
              <td><a href="#">2-18-2014</a></td>
              <td><a href="#">2-18-2014</a></td>
            </tr>
          </tbody>
        </table>
        <a class="button tiny" href="#">View All</a>
      </div>
    </section>
    <section id="mail-chimp" class="row">
      <div class="large-12 columns">
        <h4>Mail Chimp Statistics</h4>
      </div>
    </section>
    <section id="users" class="row">
      <div class="large-12 columns">
        <h4>User List</h4>
        <table>
          <thead>
            <tr>
              <th>Type</th>
              <th width="275">Practice ID</th>
              <th width="275">Created</th>
              <th width="275">Updated</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Practice</td>
              <td><a href="#">MN55930G</a></td>
              <td><a href="#">2-18-2014</a></td>
              <td><a href="#">2-18-2014</a></td>
            </tr>
            <tr>
              <td>Associate</td>
              <td><a href="#">MN55930h</a></td>
              <td><a href="#">2-18-2014</a></td>
              <td><a href="#">2-18-2014</a></td>
            </tr>
            <tr>
              <td>Practice</td>
              <td><a href="#">MN55930i</a></td>
              <td><a href="#">2-18-2014</a></td>
              <td><a href="#">2-18-2014</a></td>
            </tr>
          </tbody>
        </table>
      </div>
    </section>
    <?php include 'footer.php'; ?>

    <script src="bower_components/jquery/dist/jquery.min.js"></script>
    <script src="bower_components/foundation/js/foundation.min.js"></script>
    <script src="js/app.js"></script>
  </body>
</html>
