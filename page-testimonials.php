<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Transdent</title>
    <link rel="stylesheet" href="stylesheets/app.css" />
    <script src="bower_components/modernizr/modernizr.js"></script>
  </head>
  <body>
    <?php include 'header.php'; ?>
    <div id="main" class="row">
      <div class="large-3 columns">
        <ul class="side-nav">
           <li><a href="#">About Us</a></li>
          <li><a href="aboutus.php">Our Approach</a></li>
          <li><a href="page-team.php">Our Team &amp; Experience</a></li>
          <li class="active"><a href="page-testoimonials.php">Client Testimonials</a></li>
        </ul>
        <div class="panel">
          <h4>Call to Action 1</h4>
          <p>Donec id elit non mi porta gravida at eget metus. Aenean lacinia bibendum nulla sed consectetur.</p>
        </div>
        <div class="panel">
          <h4>Call to Action 2</h4>
          <p>Donec id elit non mi porta gravida at eget metus. Aenean lacinia bibendum nulla sed consectetur.</p>
        </div>
      </div>
      <div class="large-9 columns">
        <div class="row">
          <h1 class="large-12 columns">Client Testimonials</h1>
        </div>
        <div class="panel">
          <h2>Media</h2><br><br>
          <p>This space holds a video or image as desired.  TOTALLY OPTIONAL.</p>
        </div>
        <div class="content">
          <p>Nulla vitae elit libero, a pharetra augue. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Donec ullamcorper nulla non metus auctor fringilla. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Sed posuere consectetur est at lobortis. Morbi leo risus, porta ac consectetur ac, vestibulum at eros.</p>

        </div>
        <div class="large-12 columns">
          <blockquote>Those people who think they know everything are a great annoyance to those of us who do.<cite>Isaac Asimov</cite></blockquote>
        </div>
        <div class="large-12 columns">
          <blockquote>Those people who think they know everything are a great annoyance to those of us who do.<cite>Isaac Asimov</cite></blockquote>
        </div>
      </div>
    </div>
    <?php include 'footer.php'; ?>

    <script src="bower_components/jquery/dist/jquery.min.js"></script>
    <script src="bower_components/foundation/js/foundation.min.js"></script>
    <script src="js/app.js"></script>
  </body>
</html>
